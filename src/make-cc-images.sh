#!/bin/bash

mkdir -p cc-images

for index in $(seq 10 10 600); do
    for hue in 0.000 0.125 0.250 0.375 0.500 0.625 0.750 0.875; do
	for M in standard equivariant; do
	    python3 runmodel.py --data-dir ~/dataset/NUS ../data/NUS-test-fold1.txt \
		    ../models/NUS/NUS-$M-r1e-3-f1/model-last.pt $index -H $hue -N 11
	    n=$(printf "%05d" $index)
	    mv inputs.png cc-images/$n-$hue.png
	    mv outputs.png cc-images/$n-$hue-$M.png
	done
    done
done
