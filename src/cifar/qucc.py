import sys
sys.path.append("../src")
import torch
import numpy as np
import quccmodel as model
import ptcolor
import quccprocessing as processing


MODEL_FILE = "ilsvrc12-eg.pt"
SIZE = 256


class QUCC:
    def __init__(self, device="cpu"):
        model_data = torch.load(MODEL_FILE, map_location="cpu")
        input_code = model_data["args"].input
        output_code = model_data["args"].output
        self.net = model.CCNet(input_code=input_code, output_code=output_code, noise=0.0)
        self.net.load_state_dict(model_data["model"])
        self.device = device
        self.net.to(device)
        self.net.eval()

    def process(self, fullres):
        """Process an image

        fullres is in [0,1].
        Return inpute image, processed image and weights all in [0,1].
        """
        fullres_ng = ptcolor.remove_gamma(fullres)
        rgb = torch.nn.functional.interpolate(fullres_ng, (SIZE, SIZE),
                                              mode="bilinear", align_corners=True)
        with torch.no_grad():
            _, _, weights = self.net(rgb.to(self.device))
            weights = weights * (rgb.min(1, keepdim=True)[0] > 0).float()
            estimate = self.net.make_estimate(rgb, weights)
            balanced = processing.apply_correction(fullres_ng, estimate)
            balanced = ptcolor.apply_gamma(balanced)
        return balanced.to(fullres.device)


if __name__ == "__main__":
    qucc = QUCC()
    params = qucc.net.parameters()
    print(sum(p.numel() for p in params), "parameters")
    x = torch.rand(2, 3, 224, 224)
    # import torchvision
    # x = torchvision.io.read_image("a.png")[None, :3, :, :] / 255.0
    y = qucc.process(x)
    # torchvision.io.write_png((y[0] * 255).to(torch.uint8), "b.png")
    print(x.size(), "->", y.size())
