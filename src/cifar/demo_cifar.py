#!/usr/bin/env python3

import argparse
import torch
import torchvision
import numpy as np
import imgdata
import ptcolor


CLASSES = "plane car bird cat deer dog frog horse ship truck".split()


def parse_args():
    parser = argparse.ArgumentParser("Eval a model on CIFAR-10 images")
    a = parser.add_argument
    a("model", help="Model file")
    a("index", type=int, help="Image index")
    a("-H", "--hue", type=float, default=0.05, help="Illuminant hue")
    a("-b", "--batch-size", type=int, default=128, help="Images per minibatch")
    a("-r", "--data-dir", default="./data", help="Directory for the storage of images")
    a("-w", "--workers", type=int, default=0, help="Parallel workers")
    dev = ("cuda" if torch.cuda.is_available() else "cpu")
    a("-d", "--device", default=dev, help="Computing device")
    a("-e", "--equivariant", action="store_true", help="Use the equivariant version of the model")
    return parser.parse_args()


def main():
    args = parse_args()

    # Load the model
    data = torch.load(args.model, map_location=args.device)
    net = imgdata.make_model(data["args"].depth, args.equivariant)
    net.load_state_dict(data["model_state_dict"])
    net.to(args.device)
    net.eval()
    
    if args.equivariant:
        mean = imgdata.CIFAR10_LOGRGB_MEAN
        std = imgdata.CIFAR10_LOGRGB_STD
    else:
        mean = imgdata.CIFAR10_MEAN
        std = imgdata.CIFAR10_STD
    
    # Process a single image
    ims = []
    saturations = [x / 10 for x in range(10)]
    for saturation in saturations:
        loader = imgdata.make_cifar_loader(False, args, saturation, args.equivariant,
                                           illuminant_hue=args.hue)
        dataset = loader.dataset
        # if not ims:  # Save a sample
        #     disp = [dataset[i][0] * torch.tensor(std).view(3, 1, 1) + torch.tensor(mean).view(3, 1, 1)
        #             for i in range(args.index, args.index + 100)]
        #     torchvision.utils.save_image(torch.stack(disp, 0), "cifar.png", nrow=10)
        im = dataset[args.index][0]
        ims.append(im)
    x = torch.stack(ims, 0).to(args.device)

    with torch.no_grad():
        scores = net(x)
        probs = torch.softmax(scores, 1)
        x1 = x.to("cpu")
        x1 = x1 * torch.tensor(std).view(1, 3, 1, 1)
        x1 = x1 + torch.tensor(mean).view(1, 3, 1, 1)
    torchvision.utils.save_image(x1, "images.png", nrow=x1.size(0),
                                 padding=0)

    k = 3
    for n, s in enumerate(saturations):
        ps = probs[n, :].cpu().numpy()
        idx = np.argsort(ps)[-1:-(k + 1):-1]
        for id_ in idx:
            print(s, CLASSES[id_], ps[id_])


if __name__ == "__main__":
    main()
