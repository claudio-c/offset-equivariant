#!/usr/bin/env python3


""""Find examples in CIFAR-10 test images where

1) the original image is correctly classified with high confidence
by the standard model;
2) the same image + an illuminant is misclassified;
3) the equivariant version correctly classifies the original image.
"""


import argparse
import torch
import torchvision
import numpy as np
import imgdata
import ptcolor


CLASSES = "plane car bird cat deer dog frog horse ship truck".split()


def parse_args():
    parser = argparse.ArgumentParser("Eval a model on CIFAR-10 images")
    a = parser.add_argument
    a("standard_model", help="Model file")
    a("equivariant_model", help="Model file")
    a("-H", "--hue", type=float, default=0.05, help="Illuminant hue")
    a("-b", "--batch-size", type=int, default=128, help="Images per minibatch")
    a("-r", "--data-dir", default="./data", help="Directory for the storage of images")
    a("-w", "--workers", type=int, default=0, help="Parallel workers")
    dev = ("cuda" if torch.cuda.is_available() else "cpu")
    a("-d", "--device", default=dev, help="Computing device")
    return parser.parse_args()


def process(args, model, equivariant, saturation, look_for_good):
    data = torch.load(model, map_location=args.device)
    net = imgdata.make_model(data["args"].depth, equivariant)
    net.load_state_dict(data["model_state_dict"])
    net.to(args.device)
    net.eval()

    loader = imgdata.make_cifar_loader(False, args, saturation, equivariant,
                                       illuminant_hue=args.hue)
    result = set()
    index = 0
    for imgs, labels in loader:
        imgs = imgs.to(args.device)
        with torch.no_grad():
            scores = net(imgs).cpu()
            preds = scores.argmax(1)
            probs = torch.softmax(scores, 1)
        for i in range(imgs.size(0)):
            if ((look_for_good and labels[i] == preds[i] and probs[i, preds[i]] > 0.9) or
                ((not look_for_good) and labels[i] != preds[i])):
                result.add(index)
            index += 1
    return result


def save_images(args, indices):
    loader = imgdata.make_cifar_loader(False, args, 0.0, False,
                                       illuminant_hue=args.hue)
    imgs = []
    index = 0
    indices = set(indices)
    for img, labels in loader:
        for im in img:
            if index in indices:
                imgs.append(im)
            index += 1
    nrow = int(len(imgs) ** 0.5)
    torchvision.utils.save_image(imgs, "selection.png", nrow=nrow, padding=0)


def main():
    args = parse_args()

    # 1) look for unaltered images (S = 0) correctly classified by the standard model.
    step1 = process(args, args.standard_model, False, 0.0, True)
    print("Step1:", len(step1))
    # 2) look for distorted images (S = 0.5) misclassified by the standard model.
    step2 = process(args, args.standard_model, False, 0.5, False)
    print("Step2:", len(step2))
    # 3) look for unaltered images (S = 0) correctly classified by the equivariant model.
    step3 = process(args, args.equivariant_model, True, 0.0, True)
    print("Step3:", len(step3))

    selection = step1 & step2 & step3
    print("Selection:", len(selection))
    print(*sorted(selection))
    save_images(args, selection)


if __name__ == "__main__":
    main()
