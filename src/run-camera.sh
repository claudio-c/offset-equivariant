#!/bin/bash


for C in $(cut -d/ -f1 ../data/NUS.txt | sort | uniq); do
    for M in equivariant standard normalized; do
	echo $C $M
	echo
	DATAROOT=/home/cusano/dataset/NUS
	TRAINFILE=../data/NUS-train-${C}.txt
	VALIDFILE=../data/NUS-test-${C}.txt
	OUTFILE=NUS-bycamera/NUS-${C}-${M}
	
	./train.py --learning-rate 1e-2 --epochs 2500 \
       		   --data-dir $DATAROOT \
		   $TRAINFILE \
		   --validation-file $VALIDFILE \
		   ../models/$OUTFILE --model-type ${M}
	echo "OK..."
	sleep 10
	echo
    done
done
