#!/bin/bash


F=1
M=standard
# DATAROOT=/home/cusano/dataset/colorchecker/pngs
# TRAINFILE=../data/colorchecker-train-fold${F}.txt
# VALIDFILE=../data/colorchecker-test-fold${F}.txt
# OUTFILE=CC/CC-${M}-r$R-f${F}
DATAROOT=/home/cusano/dataset/NUS
TRAINFILE=../data/NUS-train-fold${F}.txt
VALIDFILE=../data/NUS-test-fold${F}.txt
OUTFILE=NUS/NUS-${M}-r$R-f${F}

# CC:  R in 3e-4 1e-4 
# NUS:  R in 1e-3 3e-4

for F in 2 3; do
# for F in 1 2 3; do
    TRAINFILE=../data/NUS-train-fold${F}.txt
    VALIDFILE=../data/NUS-test-fold${F}.txt
    for M in equivariant standard; do
	# for R in 1e-5 3e-5 1e-4 3e-4 1e-3 3e-3; do
	for R in 1e-3; do
	    # OUTFILE=CC/CC-${M}-r$R-f${F}
	    OUTFILE=NUS/NUS-${M}-r$R-f${F}
	    ./train.py --learning-rate $R --epochs 2500 \
       		       --data-dir $DATAROOT \
		       --model-type $M \
		       $TRAINFILE \
		       --validation-file $VALIDFILE \
		       ../models/$OUTFILE
	done
    done
done
exit

DATAROOT=/home/cusano/dataset/colorchecker/pngs
TRAINFILE=../data/colorchecker-train-fold${F}.txt
VALIDFILE=../data/colorchecker-test-fold${F}.txt
OUTFILE=CC/CC-${M}-r$R-f${F}


for F in 2 3; do
    for M in equivariant standard; do
	# for R in 1e-5 3e-5 1e-4 3e-4 1e-3 3e-3; do
	for R in 1e-4 3e-4 1e-3; do
	    OUTFILE=CC/CC-${M}-r$R-f${F}
	    # OUTFILE=NUS/NUS-${M}-r$R-f${F}
	    ./train.py --learning-rate $R --epochs 2500 \
       		       --data-dir $DATAROOT \
		       --model-type $M \
		       $TRAINFILE \
		       --validation-file $VALIDFILE \
		       ../models/$OUTFILE
	done
    done
done


# # for F in 1 2 3; do
# for F in 1; do
#     for M in equivariant standard normalized; do

# 	# DATAROOT=/home/cusano/dataset/colorchecker/pngs
# 	# TRAINFILE=../data/colorchecker-train-fold${F}.txt
# 	# VALIDFILE=../data/colorchecker-test-fold${F}.txt
# 	# OUTFILE=CC/CC-SGDSCHED5K-f${F}-${M}
# 	DATAROOT=/home/cusano/dataset/NUS
# 	TRAINFILE=../data/NUS-train-fold${F}.txt
# 	VALIDFILE=../data/NUS-test-fold${F}.txt
# 	OUTFILE=NUS/NUS-wnobn-f${F}-${M}
	
# 	./train.py --learning-rate 1e-2 --epochs 2500 \
#        		   --data-dir $DATAROOT \
# 		   $TRAINFILE \
# 		   --validation-file $VALIDFILE \
# 		   ../models/$OUTFILE --model-type ${M}
# 	echo "OK"
# 	sleep 10
# 	echo
#     done
# done


# # for F in 1 2 3; do
# for F in 1; do
#     for M in equivariant standard normalized; do

# 	DATAROOT=/home/cusano/dataset/colorchecker/pngs
# 	TRAINFILE=../data/colorchecker-train-fold${F}.txt
# 	VALIDFILE=../data/colorchecker-test-fold${F}.txt
# 	OUTFILE=CC/CC-wnobn-SGDSCHED5K-f${F}-${M}
# 	# DATAROOT=/home/cusano/dataset/NUS
# 	# TRAINFILE=../data/NUS-train-fold${F}.txt
# 	# VALIDFILE=../data/NUS-test-fold${F}.txt
# 	# OUTFILE=NUS/NUS-f${F}-${M}
	
# 	./train.py --learning-rate 1e-2 --epochs 2500 \
#        		   --data-dir $DATAROOT \
# 		   $TRAINFILE \
# 		   --validation-file $VALIDFILE \
# 		   ../models/$OUTFILE --model-type ${M}
# 	echo "OK"
# 	sleep 10
# 	echo
#     done
# done



# for M in equivariant standard normalized; do
#     for R in 1e-2 3e-2 1e-1 3e-1; do

# 	F=1
# 	DATAROOT=/home/cusano/dataset/colorchecker/pngs
# 	TRAINFILE=../data/colorchecker-train-fold${F}.txt
# 	VALIDFILE=../data/colorchecker-test-fold${F}.txt
# 	OUTFILE=CC-SGD-f${F}-${M}-lr${R}
	
# 	./train.py --learning-rate $R --epochs 7500 \
#        		   --data-dir $DATAROOT \
# 		   $TRAINFILE \
# 		   --validation-file $VALIDFILE \
# 		   ../models/$OUTFILE --model-type ${M}
#     done
# done
