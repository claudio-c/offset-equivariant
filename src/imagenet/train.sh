#/bin/sh

python3 mainmod.py -b 48 --gpu 0 -a resnet50 ~/dataset/ilsvrc12 \
	--aug-saturation 0.3 \
	--aug-hue 0.3 \
	--aug-contrast 0.3 \
	--aug-brightness 0.3
