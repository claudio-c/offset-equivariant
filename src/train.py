#!/usr/bin/env python3

import torch
import torchvision
import torch.utils.tensorboard
import numpy as np
# import model
import cerberus
import ecerberus
import cccnn
import ecccnn
import ccresnet
import eccresnet
import data
import color
import argparse
import os


def parse_args():
    parser = argparse.ArgumentParser("Train a model")
    a = parser.add_argument
    a("training_file", help="File containing training data")
    a("model_dir", help="Directory that will contain the trained model")
    a = parser.add_argument_group("Optimization").add_argument
    a("--batch", type=int, default=256, help="Size of the mini batches")
    dev = ("cuda" if torch.cuda.is_available() else "cpu")
    a("--device", default=dev, help="Computing device")
    a("--workers", type=int, default=4, help="Number of parallel workers")
    a("--learning-rate", type=float, default=1e-4, help="Learning rate")
    a("--weight-decay", type=float, default=1e-5, help="Weight decay")
    a("--epochs", type=int, default=300, help="Training epochs")
    a = parser.add_argument_group("Data").add_argument
    a("--data-dir", help="Base directory containing images")
    a("--image-size", type=int, default=64, help="Size of the images")
    a = parser.add_argument_group("Validation").add_argument
    a("--validation-file", help="File containing validation data")
    a("--validate-every", type=int, default=10, help="Frequency of validation (epochs)")
    a("--display-every", type=int, default=50, help="Frequency of display (epochs)")
    a = parser.add_argument_group("Architecture").add_argument
    archs = ["cerberus", "cccnn", "resnet"]
    a("--architecture", choices=archs, default=archs[0], help="Architecture")
    types = ["equivariant", "standard"]   # , "normalized"]
    a("--model-type", choices=types, default=types[0], help="Model type")
    return parser.parse_args()


def mae_loss(estimate, target, reduction="middle"):
    diff = (target - estimate).abs()
    if reduction == "mean":
        return diff.mean()
    elif reduction == "none":
        return diff
    elif reduction == "middle":
        # In the 'Color Cerberus' paper the worst 20% and the best 1%
        # are excluded from the loss
        low = torch.quantile(diff, 0.01)
        hi = torch.quantile(diff, 0.8)
        mask = ((diff >= low) & (diff <= hi)).float()
        return (diff * mask).sum() / mask.sum()
    else:
        raise ValueError("Unknown reduction mode")


def training_epoch(net, optimizer, loader, args):
    tot_loss = 0
    tot_angerr = 0
    tot_reperr = 0
    steps = 0
    net.train()
    for rgb, ill_rgb in loader:
        rgb = rgb.to(args.device)
        ill_rgb = ill_rgb.to(args.device)
        optimizer.zero_grad()
        estimate = net(rgb)
        loss = mae_loss(estimate, ill_rgb)
        angerr = color.angular_error(estimate, ill_rgb).mean()
        reperr = color.reproduction_error(estimate, ill_rgb).mean()
        loss.backward()
        optimizer.step()
        steps += 1
        tot_loss += loss.item()
        tot_angerr += angerr.item()
        tot_reperr += reperr.item()
    return steps, tot_loss / steps, tot_angerr / steps, tot_reperr / steps


def display_image(net, loader, args, nrow=6, ncol=8):
    net.eval()
    for rgb, ill_rgb in loader:
        rgb = rgb[:nrow * ncol].to(args.device)
        ill_rgb = ill_rgb[:nrow * ncol].to(args.device)
        break
    bsz = rgb.size(0)
    estimate = net(rgb)
    corrected = rgb / estimate.view(bsz, 3, 1, 1)
    norm = corrected.view(bsz, -1).max(-1).values.view(bsz, 1, 1, 1)
    corrected = corrected / norm
    border = ill_rgb / estimate
    border = torch.clamp(border / border.max(-1, True).values, 0, 1).view(bsz, 3, 1, 1)
    padsize = (10, 10, 10, 10)
    corrected = torch.nn.functional.pad(corrected - border, padsize) + border
    corrected = torch.clamp(color.apply_gamma(corrected), 0, 1)
    return torchvision.utils.make_grid(corrected, nrow=nrow)


def validation_epoch(net, loader, args):
    l2s = []
    angerrs = []
    net.eval()
    for rgb, ill_rgb in loader:
        rgb = rgb.to(args.device)
        ill_rgb = ill_rgb.to(args.device)
        estimate = net(rgb)
        l2 = mae_loss(estimate, ill_rgb, reduction="none")
        l2s.extend(l2.tolist())
        angerr = color.reproduction_error(estimate, ill_rgb)
        angerrs.extend(angerr.tolist())

    l2s = np.array(l2s)
    angerrs = np.array(angerrs)
    metrics = {
        "mse": l2s.mean(),
        "ang_max": angerrs.max(),
        "ang_mean": angerrs.mean(),
        "ang_median": np.median(angerrs)
    }
    return metrics


def save_model(filename, net, optimizer, steps, args):
    path = os.path.join(args.model_dir, filename)
    tosave = {
        "steps": steps,
        "model": net.state_dict(),
        "optimizer": optimizer.state_dict(),
        "args": args
    }
    torch.save(tosave, path)


def main():
    args = parse_args()

    # Data
    train_loader = data.make_loader(args.data_dir, args.training_file, True,
                                    batch_size=args.batch,
                                    num_workers=args.workers,
                                    imagesize=args.image_size)
    if args.validation_file:
        val_loader = data.make_loader(args.data_dir, args.validation_file,
                                      False,
                                      batch_size=args.batch,
                                      num_workers=args.workers,
                                      imagesize=args.image_size)

    # Model and optimizer.
    if args.architecture == "cerberus":
        if args.model_type == "standard":
            net = cerberus.Cerberus(1)
        elif args.model_type == "equivariant":
            net = ecerberus.ECerberus(1)
    elif args.architecture == "cccnn":
        if args.model_type == "standard":
            net = cccnn.CCCNN()
        elif args.model_type == "equivariant":
            net = ecccnn.ECCCNN()
    elif args.architecture == "resnet":
        if args.model_type == "standard":
            net = ccresnet.CCResnet()
        elif args.model_type == "equivariant":
            net = eccresnet.ECCResnet()
    net.to(args.device)
    params = list(net.parameters())
    optimizer = torch.optim.Adam(params, lr=args.learning_rate,
                                 weight_decay=args.weight_decay)
    # optimizer = torch.optim.SGD(params, lr=args.learning_rate,
    #                             weight_decay=args.weight_decay, momentum=0.9,
    #                             weight_decay=args.weight_decay)
    print("{:.2f}M parameters".format(sum(p.numel() for p in params) * 1e-6))
    print()
    del params
    # scheduler = torch.optim.lr_scheduler.StepLR(optimizer, step_size=1000, gamma=0.1)

    # Tensorboard.
    writer = torch.utils.tensorboard.SummaryWriter(args.model_dir)

    # Main loop
    steps = 0
    for epoch in range(1, args.epochs + 1):
        s, loss, angerr, reperr = training_epoch(net, optimizer, train_loader, args)
        steps += s
        # scheduler.step()
        writer.add_scalar("loss/train", loss, steps)
        writer.add_scalar("angular-mean/train", angerr, steps)
        print(f"{epoch:3d}  {loss:.4f}  {angerr:.4f}  {reperr:.4f}")
        if args.validation_file and epoch % args.validate_every == 0:
            metrics = validation_epoch(net, val_loader, args)
            print(f"MSE  {metrics['mse']:.4f}   " +
                  f"MEDIAN  {metrics['ang_median']:.4f}   " +
                  f"MEAN  {metrics['ang_mean']:.4f}   " +
                  f"MAX  {metrics['ang_max']:.4f}   ")
            writer.add_scalar("loss/test", metrics["mse"], steps)
            writer.add_scalar("angular-mean/test", metrics["ang_mean"], steps)
            writer.add_scalar("angular-median/test", metrics["ang_median"], steps)
            writer.add_scalar('angular-max/test', metrics["ang_max"], steps)
        if args.validation_file and epoch % args.display_every == 0:
            disp = display_image(net, val_loader, args)
            writer.add_image("test/display", disp, steps)
        save_model("model-last.pt", net, optimizer, steps, args)


if __name__ == "__main__":
    main()
