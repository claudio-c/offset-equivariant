import torch


def add_offset(x, offset):
    g = offset.size(1)
    y = x.view(x.size(0), g, -1) + offset.unsqueeze(-1)
    return y.view_as(x)


class Equivariant(torch.nn.Module):
    def __init__(self, groups, operation):
        super().__init__()
        self.groups = groups
        self.operation = operation

    def forward(self, x):
        m = x.view(x.size(0), self.groups, -1).mean(2)
        y = self.operation(add_offset(x, -m))
        return add_offset(y, m)


class LinearEquivariant(torch.nn.Module):
    def __init__(self, groups, channels, operation):
        super().__init__()
        self.groups = groups
        self.channels = channels
        self.operation = operation
        w = torch.zeros(groups, channels // groups)
        torch.nn.init.normal_(w, std=1e-4)
        self.weight = torch.nn.Parameter(w)

    def forward(self, x):
        n = self.channels // self.groups
        y = x.view(x.size(0), self.groups, n, -1).mean(3)
        avg = y.mean(2)
        mult = torch.einsum("bgc,gc->bg", y, self.weight)
        sum_w = self.weight.sum(1).unsqueeze(0)
        m = avg + mult - avg * sum_w
        z = self.operation(add_offset(x, -m))
        return add_offset(z, m)
    

class ConvBlock(torch.nn.Module):
    def __init__(self, inch, outch, modifier):
        super().__init__()
        self.conv1 = modifier(torch.nn.Conv2d(inch, outch, 3, 1, padding=1), inch)
        self.relu1 = modifier(torch.nn.ReLU(), outch)
        # self.bn1 = modifier(torch.nn.BatchNorm2d(outch), outch)
        self.bn1 = torch.nn.Identity()
        self.conv2 = modifier(torch.nn.Conv2d(outch, outch, 3, 2, padding=1), outch)
        self.relu2 = modifier(torch.nn.ReLU(), outch)
        # self.bn2 = modifier(torch.nn.BatchNorm2d(outch), outch)
        self.bn2 = torch.nn.Identity()

    def forward(self, x):
        x = self.bn1(self.relu1(self.conv1(x)))
        x = self.bn2(self.relu2(self.conv2(x)))
        return x


# class EstimationNet(torch.nn.Module):
#     def __init__(self, equivariant=True):
#         super().__init__()

#         def modifier(m):
#             return Equivariant(3, m) if equivariant else m
#         self.conv = modifier(torch.nn.Conv2d(3, 18, 7, 4, padding=3))
#         self.relu = modifier(torch.nn.ReLU())
#         self.bn = modifier(torch.nn.BatchNorm2d(18))
#         self.block1 = ConvBlock(18, 36, modifier)
#         self.block2 = ConvBlock(36, 72, modifier)
#         self.block3 = ConvBlock(72, 144, modifier)
#         self.pool = torch.nn.AdaptiveAvgPool2d(1)
#         self.flatten = torch.nn.Flatten()
#         self.linear = modifier(torch.nn.Linear(144, 3))

#     def forward(self, x):
#         x = self.bn(self.relu(self.conv(x)))
#         x = self.block1(x)
#         x = self.block2(x)
#         x = self.block3(x)
#         x = self.pool(x)
#         x = self.linear(self.flatten(x))
#         return x


class EstimationNet(torch.nn.Module):
    def __init__(self, equivariant=True):
        super().__init__()

        def modifier(m, c):
            return LinearEquivariant(3, c, m) if equivariant else m
        self.conv = modifier(torch.nn.Conv2d(3, 18, 7, 4, padding=3), 3)
        self.relu = modifier(torch.nn.ReLU(), 18)
        # self.bn = modifier(torch.nn.BatchNorm2d(18), 18)
        self.bn = torch.nn.Identity()
        self.block1 = ConvBlock(18, 36, modifier)
        self.block2 = ConvBlock(36, 72, modifier)
        self.block3 = ConvBlock(72, 144, modifier)
        self.pool = torch.nn.AdaptiveAvgPool2d(1)
        self.flatten = torch.nn.Flatten()
        self.linear = modifier(torch.nn.Linear(144, 3), 144)

    def forward(self, x):
        x = self.bn(self.relu(self.conv(x)))
        x = self.block1(x)
        x = self.block2(x)
        x = self.block3(x)
        x = self.pool(x)
        x = self.linear(self.flatten(x))
        return x


def make_model(tag):
    if tag == "equivariant":
        return EstimationNet(True)
    elif tag == "standard":
        return EstimationNet(False)
    elif tag == "normalized":
        # return Equivariant(3, EstimationNet(False))  # !!!
        return LinearEquivariant(3, 3, EstimationNet(False))
    else:
        raise ValueError("Unknown model type")


def _test():
    net = make_model("equivariant")
    # net.eval()
    x = torch.rand(5, 3, 256, 256)
    y = net(x)
    print(x.size(), "->", y.size())
    n = sum(p.numel() for p in net.parameters())
    print(f"{n * 1e-6:.3f}M parameters")
    offset = torch.tensor([1, -2, 3]).unsqueeze(0)
    y2 = net(add_offset(x, offset))
    diff = (y2 - add_offset(y, offset)).abs().max().item()
    print("Equiv diff.", diff)


def _test_linear():
    net = make_model("equivariant")
    # net = LinearEquivariant(3, 12, torch.nn.BatchNorm2d(12))
    # net.eval()
    x = torch.rand(5, 3, 256, 256)
    y = net(x)
    print(x.size(), "->", y.size())
    n = sum(p.numel() for p in net.parameters())
    print(f"{n * 1e-6:.3f}M parameters")
    offset = torch.tensor([1, -2, 3]).unsqueeze(0)
    y2 = net(add_offset(x, offset))
    diff = (y2 - add_offset(y, offset)).abs().max().item()
    print("Equiv diff.", diff)


if __name__ == "__main__":
    _test()
    # _test_linear()
