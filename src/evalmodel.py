#!/usr/bin/env python3

import torch
import cerberus
import ecerberus
import cccnn
import ecccnn
import ccresnet
import eccresnet
import data
import argparse
import train


def parse_args():
    parser = argparse.ArgumentParser("Evaluate a model")
    a = parser.add_argument
    a("test_file", help="File containing test data")
    a("model_file", help="File with the parameters of the trained model")
    a = parser.add_argument_group("Optimization").add_argument
    a("--batch", type=int, default=256, help="Size of the mini batches")
    dev = ("cuda" if torch.cuda.is_available() else "cpu")
    a("--device", default=dev, help="Computing device")
    a("--workers", type=int, default=4, help="Number of parallel workers")
    a = parser.add_argument_group("Data").add_argument
    a("--data-dir", help="Base directory containing images")
    a("--image-size", type=int, default=64, help="Size of the images")
    a("-s", "--lluminant-saturation", type=float, default=0.0,
      help="Saturation of random illuminant")
    return parser.parse_args()


def load_model(filename):
    data = torch.load(filename)
    arch = getattr(data["args"], "architecture", "cerberus")
    model_type = data["args"].model_type
    if arch == "cerberus":
        if model_type == "standard":
            net = cerberus.Cerberus(1)
        elif model_type == "equivariant":
            net = ecerberus.ECerberus(1)
    elif arch == "cccnn":
        if model_type == "standard":
            net = cccnn.CCCNN()
        elif model_type == "equivariant":
            net = ecccnn.ECCCNN()
    elif arch == "resnet":
        if model_type == "standard":
            net = ccresnet.CCResnet()
        elif model_type == "equivariant":
            net = eccresnet.ECCResnet()
    net.load_state_dict(data["model"])
    return net


def main():
    args = parse_args()

    # Data
    torch.manual_seed(1112)
    test_loader = data.make_loader(args.data_dir, args.test_file,
                                   False,
                                   batch_size=args.batch,
                                   num_workers=args.workers,
                                   imagesize=args.image_size,
                                   illuminant_saturation=args.lluminant_saturation)

    # Model
    net = load_model(args.model_file)
    net.to(args.device)

    metrics = train.validation_epoch(net, test_loader, args)
    print(f"MSE  {metrics['mse']:.4f}   " +
          f"MEDIAN  {metrics['ang_median']:.4f}   " +
          f"MEAN  {metrics['ang_mean']:.4f}   " +
          f"MAX  {metrics['ang_max']:.4f}   ")


if __name__ == "__main__":
    main()
