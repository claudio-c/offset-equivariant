import torch
import offsetequiv as oe


class ECCCNN(torch.nn.Module):
    """Equivariant version of the CCCNN model."""
    def __init__(self):
        super().__init__()
        self.main = torch.nn.Sequential(
            torch.nn.AdaptiveAvgPool2d(64),
            oe.EquivariantConv2d(3, 240, 3, 1, padding=0),
            oe.EquivariantReLU(3),
            torch.nn.MaxPool2d(16),
            torch.nn.Flatten(1),
            oe.EquivariantLinear(240 * 4 * 4, 42, 3),
            oe.EquivariantReLU(3),
            oe.EquivariantLinear(42, 3, 3)
        )

    def forward(self, x):
        return self.main(x)


def _hook(module, input_):
    print(*input_[0].size())


def _test():
    net = ECCCNN()
    for m in net.modules():
        m.register_forward_pre_hook(_hook)
    ps = sum(p.numel() for p in net.parameters())
    offset = 0.5 + 0.5 * torch.rand(1, 3)
    print(net)
    print(f"{ps / 1000:.1f}K paramters ({ps})")
    x = torch.rand(1, 3, 64, 64)
    y1 = net(x) * offset
    y2 = net(x * offset[: , :, None, None])
    print(x.size(), "->", y1.size())
    if torch.allclose(y1, y2):
        print("OK")
    else:
        print("NON OK")
        print("y1", y1)
        print("y2", y2)
        print((y2 - y1).abs().max())


if __name__ == "__main__":
    _test()
