#!/bin/bash


#ARCH=cccnet
ARCH=resnet


for F in 1 2 3; do
    # for M in standard equivariant; do
    for M in equivariant; do
	echo $M $F
	./train.py --architecture $ARCH \
		   --model-type $M --learning-rate 1e-3 \
		   --data-dir ~/dataset/NUS \
		   --image-size 64 \
		   --epochs 8000 \
		   --batch 128 \
		   --validation-file ../data/NUS-test-fold$F.txt \
		   ../data/NUS-train-fold$F.txt \
		   ../models/NUS-$ARCH/NUS-$M-r1e-3-f$F
    done
done
