#!/usr/bin/env python3

import torch
import torchvision
import cerberus
import ecerberus
import data
import argparse
import train
import sys
sys.path.append("cifar")
import ptcolor


def parse_args():
    parser = argparse.ArgumentParser("Evaluate a model")
    a = parser.add_argument
    a("test_file", help="File containing test data")
    a("model_file", help="File with the parameters of the trained model")
    a("image_index", type=int, help="Index of input image")
    a = parser.add_argument_group("Optimization").add_argument
    dev = ("cuda" if torch.cuda.is_available() else "cpu")
    a("--device", default=dev, help="Computing device")
    a = parser.add_argument_group("Data").add_argument
    a("--data-dir", help="Base directory containing images")
    a("--image-size", type=int, default=64, help="Size of the images")
    a("-H", "--illuminant-hue", type=float, default=0.0,
      help="Hue of the illuminant")
    a("-N", "--outputs", type=int, default=10,
      help="Number of output images")
    return parser.parse_args()


def load_model(filename):
    data = torch.load(filename)
    model_type = data["args"].model_type
    if model_type == "standard":
        net = cerberus.Cerberus(1)
    elif model_type == "equivariant":
        net = ecerberus.ECerberus(1)
    net.load_state_dict(data["model"])
    return net


def apply_illuminant(x, y, h, s):
    hsv = torch.tensor([h, s, 1.0], device=x.device).view(3, 1, 1)
    ill = ptcolor.hsv2rgb(hsv[None, ...])[0]
    return x * ill, y * ill[:, 0, 0]
    

def save_images(filename, lst):
    lst = [x / x.max() for x in lst]
    ims = torch.cat(lst, 3)
    ims = ptcolor.apply_gamma(ims)
    torchvision.utils.save_image(ims, filename)
    

def sat_values(a, b, n):
    for i in range(n):
        yield a + (b - a) * i / n

    
def main():
    args = parse_args()

    # Data
    torch.manual_seed(1112)
    test_loader = data.make_loader(args.data_dir, args.test_file,
                                   False,
                                   batch_size=1,
                                   num_workers=0,
                                   imagesize=args.image_size,
                                   illuminant_saturation=0)
    dataset = test_loader.dataset
    image, illuminant = dataset[args.image_index]
    image = image.to(args.device).unsqueeze(0)
    illuminant = illuminant.to(args.device)

    # Model
    net = load_model(args.model_file)
    net.eval()
    net.to(args.device)

    fmt = "{:.1f}" + " {:.3f}" * 6
    inputs = []
    corrected = []
    for s in sat_values(0, 1, args.outputs):
        image_mod, ill_mod = apply_illuminant(image, illuminant, args.illuminant_hue, s)
        estimate = net(image_mod)
        estimate = estimate / estimate.max()
        ill_mod = ill_mod / ill_mod.max()
        print(fmt.format(s, *ill_mod.tolist(), *estimate[0].tolist()))
        inputs.append(image_mod / image_mod.max())
        corr = image_mod / estimate.view(1, 3, 1, 1)
        corrected.append(corr / corr.max())
    save_images("gt.png", [image / illuminant.view(1, 3, 1, 1)])
    save_images("inputs.png", inputs)
    save_images("outputs.png", corrected)


if __name__ == "__main__":
    main()
