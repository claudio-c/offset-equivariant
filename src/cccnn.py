import torch


class CCCNN(torch.nn.Module):
    """Implementation of (a simplified version of) the model described in:

    'S. Bianco, C. Cusano, and R. Schettini: Color constancy using
    cnns, CVPRW 2015'.

    Input: Nx3x64x64 batch of RGB patches
    Output: Nx3 illuminant estimates

    """
    def __init__(self):
        super().__init__()
        self.main = torch.nn.Sequential(
            torch.nn.AdaptiveAvgPool2d(64),
            torch.nn.Conv2d(3, 240, 1, padding=0),
            torch.nn.ReLU(inplace=True),
            torch.nn.MaxPool2d(16),
            torch.nn.Flatten(1),
            torch.nn.Linear(240 * 4 * 4, 42),
            torch.nn.ReLU(inplace=True),
            torch.nn.Linear(42, 3)
        )

    def forward(self, x):
        return self.main(x)


def _hook(module, input_):
    print(*input_[0].size())


def _test():
    net = CCCNN()
    for m in net.modules():
        m.register_forward_pre_hook(_hook)
    ps = sum(p.numel() for p in net.parameters())
    print(net)
    print(f"{ps / 1000:.1f}K paramters ({ps})")
    x = torch.rand(4, 3, 64, 64)
    y = net(x)
    print(x.size(), "->", y.size())


if __name__ == "__main__":
    _test()
