#!/bin/bash

# OUT=nus
OUT=cciw
# M=standard
M=equivariant

for S in $(seq 0.0 0.1 1.0|tr , .); do
    echo -n "$S "
    for F in 1 2 3; do
	MOD=../models/NUS-resnet/NUS-${M}-r1e-3-f${F}/model-last.pt
	# MOD=../models/NUS/NUS-${M}-r1e-3-f${F}/model-last.pt
	txt=$(python3 evalmodel.py --data-dir ~/dataset/NUS --batch 128 \
		      ../data/NUS-test-fold${F}.txt $MOD -s $S)
	echo -n "$txt "
    done
    echo
done > $OUT-$M.txt
