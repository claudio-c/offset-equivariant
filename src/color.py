import torch


def rgb2yuv(rgb, eps=0.002):
    rgb = torch.clamp(rgb, eps)
    y = 0.5 * torch.log((rgb ** 2).sum(1))
    u = torch.log(rgb[:, 1] / rgb[:, 0])
    v = torch.log(rgb[:, 1] / rgb[:, 2])
    return torch.stack((y, u, v), 1)


def yuv2rgb(yuv):
    q = torch.exp(2 * yuv)
    g2 = q[:, 0] / (1 + 1 / q[:, 1] + 1 / q[:, 2])
    r2 = g2 / q[:, 1]
    b2 = g2 / q[:, 2]
    return torch.sqrt(torch.stack((r2, g2, b2), 1))


def reset_y(yuv):
    n = yuv.view(yuv.size(0), 3, -1)
    n = n * torch.tensor([0, 1, 1], device=yuv.device).view(1, 3, 1)
    return n.view_as(yuv)


def angular_error(input, target):
    c = torch.nn.functional.cosine_similarity(input, target)
    rad = torch.acos(torch.clamp(c, -1, 1))
    return torch.rad2deg(rad)


def reproduction_error(input, target):
    e = torch.ones_like(target)
    r = target / input
    return angular_error(e, r)


def apply_gamma(rgb, gamma="srgb"):
    """Linear to gamma rgb.

    Assume that rgb values are in the [0, 1] range (but values outside are tolerated).

    gamma can be "srgb", a real-valued exponent, or None.

    >>> apply_gamma(torch.tensor([0.5, 0.4, 0.1]).view([1, 3, 1, 1]), 0.5).view(-1)
    tensor([0.2500, 0.1600, 0.0100])

    """
    if gamma == "srgb":
        T = 0.0031308
        rgb1 = torch.max(rgb, rgb.new_tensor(T))
        rgb2 = torch.pow(torch.abs(rgb1), 1 / 2.4)
        return torch.where(rgb < T, 12.92 * rgb, (1.055 * rgb2 - 0.055))
    elif gamma is None:
        return rgb
    else:
        return torch.pow(torch.max(rgb, rgb.new_tensor(0.0)), 1.0 / gamma)


def remove_gamma(rgb, gamma="srgb"):
    """Gamma to linear rgb.

    Assume that rgb values are in the [0, 1] range (but values outside are tolerated).

    gamma can be "srgb", a real-valued exponent, or None.

    >>> remove_gamma(apply_gamma(torch.tensor([0.001, 0.3, 0.4])))
    tensor([0.0010,  0.3000,  0.4000])

    >>> remove_gamma(torch.tensor([0.5, 0.4, 0.1]).view([1, 3, 1, 1]), 2.0).view(-1)
    tensor([0.2500, 0.1600, 0.0100])
    """
    if gamma == "srgb":
        T = 0.04045
        rgb1 = torch.max(rgb, rgb.new_tensor(T))
        return torch.where(rgb < T, rgb / 12.92, torch.pow(torch.abs(rgb1 + 0.055) / 1.055, 2.4))
    elif gamma is None:
        return rgb
    else:
        res = torch.pow(torch.max(rgb, rgb.new_tensor(0.0)), gamma) + \
              torch.min(rgb, rgb.new_tensor(0.0))  # very important to avoid vanishing gradients
        return res


if __name__ == "__main__":
    import matplotlib.pyplot as plt
    im = plt.imread("/home/cusano/Downloads/bl/done/No.2046 Dora/0015.jpg")
    rgb = torch.tensor(im[None, ...].transpose(0, 3, 1, 2)) / 255.0
    yuv = rgb2yuv(rgb)
    yuv = yuv + torch.tensor([0, 0.5, 0.5]).view(1, 3, 1, 1)
    rgb2 = yuv2rgb(yuv)
    print(rgb2.max())
    im2 = rgb2[0].numpy().transpose(1, 2, 0)
    plt.imshow(im)
    plt.title("1")
    plt.figure()
    plt.imshow(im2)
    plt.title("2")
    plt.show()
