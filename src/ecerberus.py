import torch
import offsetequiv as oe


class ECerberus(torch.nn.Module):
    """Equivariant version of the 'Color Cerberus' model."""
    def __init__(self, heads):
        super().__init__()
        self.avg = torch.nn.AdaptiveAvgPool2d(1)
        self.main = torch.nn.Sequential(
            oe.EquivariantConv2d(6, 15, 3, 3, padding=1),
            oe.EquivariantReLU(3),
            torch.nn.MaxPool2d(2),
            oe.EquivariantConv2d(15, 33, 3, 3, padding=1),
            oe.EquivariantReLU(3),
            torch.nn.MaxPool2d(2),
            oe.EquivariantConv2d(33, 63, 3, 3, padding=1),
            oe.EquivariantReLU(3),
            torch.nn.MaxPool2d(2),
            oe.EquivariantConv2d(63, 129, 3, 3, padding=1),
            oe.EquivariantReLU(3),
            torch.nn.MaxPool2d(2),
            oe.EquivariantConv2d(129, 15, 3, 1, padding=0),
            oe.EquivariantReLU(3)
        )
        self.fc1 = oe.EquivariantLinear(240, 399, 3)
        self.relu = oe.EquivariantReLU(3)
        self.fc2 = oe.EquivariantLinear(399, 3 * heads, 3)

    def forward(self, x):
        # 1) RGB to logRGB
        x = -torch.log(torch.clamp(x, min=2e-4))
        # 2) Estimation
        y = self.avg(x).expand_as(x)
        x = torch.stack((x, y), 2).view(x.size(0), 6, x.size(2), x.size(3))
        x = self.main(x)
        x = torch.flatten(x, 1)
        x = self.fc2(self.relu(self.fc1(x)))
        # 3) logRGB to RGB
        x = torch.exp(-x)
        return x


def _hook(module, input_):
    print(*input_[0].size())


def _test():
    net = ECerberus(1)
    for m in net.modules():
        m.register_forward_pre_hook(_hook)
    ps = sum(p.numel() for p in net.parameters())
    offset = 0.5 + 0.5 * torch.rand(1, 3)
    print(net)
    print(f"{ps / 1000:.1f}K paramters ({ps})")
    x = torch.rand(1, 3, 64, 64)
    y1 = net(x) * offset
    y2 = net(x * offset[: , :, None, None])
    print(x.size(), "->", y1.size())
    if torch.allclose(y1, y2):
        print("OK")
    else:
        print("NON OK")
        print("y1", y1)
        print("y2", y2)
        print((y2 - y1).abs().max())


if __name__ == "__main__":
    _test()
