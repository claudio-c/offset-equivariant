import torch
import torchvision
import os
import PIL
import PIL.Image
import sys
sys.path.append("cifar")
import ptcolor


class ColorConstancyDataset(torch.utils.data.Dataset):
    def __init__(self, root, filelist, training, imagesize=64,
                 illuminant_saturation=0):
        self.root = root
        ps = []
        ls = []
        with open(filelist) as f:
            for line in f:
                fs = line.rsplit(maxsplit=3)
                if not fs:
                    continue
                ps.append(fs[0].strip())
                ls.append(list(map(float, fs[1:])))
        self.paths = ps
        self.illuminants = torch.tensor(ls)
        self.illuminant_saturation = illuminant_saturation
        if training:
            self.transforms = torchvision.transforms.Compose([
                torchvision.transforms.RandomResizedCrop(imagesize, scale=(0.5, 1.0),
                                                         ratio=(1.0, 1.0)),
                torchvision.transforms.RandomHorizontalFlip(),
                torchvision.transforms.ToTensor(),
            ])
        else:
            self.transforms = torchvision.transforms.Compose([
                torchvision.transforms.Resize(imagesize),
                torchvision.transforms.CenterCrop(imagesize),
                torchvision.transforms.ToTensor(),
            ])

    def _apply_random_illuminant(self, x, y):
        # This assumes linear RGB data
        if self.illuminant_saturation <= 0:
            return x, y
        hsv = torch.rand(3, 1, 1)
        hsv.data[1] = self.illuminant_saturation
        hsv.data[2] = 1.0
        ill = ptcolor.hsv2rgb(hsv[None, ...])[0]
        return x * ill, y * ill[:, 0, 0]

    def __len__(self):
        return len(self.paths)

    def __getitem__(self, index):
        path = os.path.join(self.root, self.paths[index])
        illuminant = self.illuminants[index]
        img = self.transforms(PIL.Image.open(path))
        return self._apply_random_illuminant(img, illuminant)


def make_loader(root, filelist, training, batch_size=16, num_workers=4,
                imagesize=64, illuminant_saturation=0):
    ds = ColorConstancyDataset(root, filelist, training, imagesize=imagesize,
                               illuminant_saturation=illuminant_saturation)
    loader = torch.utils.data.DataLoader(ds, batch_size, shuffle=training,
                                         num_workers=num_workers,
                                         drop_last=training)
    return loader


def _test():
    root = "/home/cusano/dataset/NUS"
    filelist = "../data/NUS.txt"
    loader = make_loader(root, filelist, True)
    for x in loader:
        for d in x:
            print("x".join(map(str, d.size())), d.dtype)
    print(x[1])
    import matplotlib.pyplot as plt
    grid = torchvision.utils.make_grid(x[0])
    print(grid.size())
    plt.imshow(grid.numpy().transpose(1, 2, 0))
    plt.show()


if __name__ == "__main__":
    _test()
