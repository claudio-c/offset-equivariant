#!/usr/bin/env python3

import torch
import numpy as np
import data
import color
import argparse


def parse_args():
    parser = argparse.ArgumentParser("Eval GW and MAXRGB")
    a = parser.add_argument
    a("training_file", help="File containing training data")
    a("--batch", type=int, default=64, help="Size of the mini batches")
    dev = ("cuda" if torch.cuda.is_available() else "cpu")
    a("--device", default=dev, help="Computing device")
    a("--workers", type=int, default=4, help="Number of parallel workers")
    a("--data-dir", help="Base directory containing images")
    a("--image-size", type=int, default="224", help="Size of the images")
    a("--start-size", type=int, default="256", help="Size of the images before cropping")
    return parser.parse_args()


def validation_epoch(loader, device):
    gw_errs = []
    maxrgb_errs = []
    for rgb, ill_rgb in loader:
        rgb = rgb.to(device)
        ill_rgb = ill_rgb.to(device)
        gw_est = rgb.mean(-1).mean(-1)
        angerr = color.angular_error(gw_est, ill_rgb)
        gw_errs.extend(angerr.tolist())
        maxrgb_est = rgb.max(-1).values.max(-1).values
        angerr = color.angular_error(maxrgb_est, ill_rgb)
        maxrgb_errs.extend(angerr.tolist())
    return gw_errs, maxrgb_errs


def main():
    args = parse_args()
    loader = data.make_loader(args.data_dir, args.training_file, False,
                              batch_size=args.batch,
                              num_workers=args.workers,
                              imagesize=args.image_size,
                              beforecropsize=args.start_size)
    gw_errs, maxrgb_errs = validation_epoch(loader, args.device)
    print("         N      MEAN    MEDIAN  MAX")
    for name, errs in (("GW", gw_errs), ("MAXRGB", maxrgb_errs)):
        errs = np.array(errs)
        print(f"{name:6s}   {len(errs)}   {errs.mean():.3f}   " +
              f"{np.median(errs):.3f}   {errs.max():.3f}")


if __name__ == "__main__":
    main()
