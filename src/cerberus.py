import torch


class Cerberus(torch.nn.Module):
    """Implementation of the model described in:

    'Color Cerberus', by A. Savchik, E. Ershov, and S. Karpenko, ISPA (219).

    Input: Nx3x64x64 batch of RGB patches
    Output: Nx(3H) illuminant estimates (H heads)

    """
    def __init__(self, heads):
        super().__init__()
        self.avg = torch.nn.AdaptiveAvgPool2d(1)
        self.main = torch.nn.Sequential(
            torch.nn.Conv2d(6, 16, 3, padding=1),
            torch.nn.ReLU(inplace=True),
            torch.nn.MaxPool2d(2),
            torch.nn.Conv2d(16, 32, 3, padding=1),
            torch.nn.ReLU(inplace=True),
            torch.nn.MaxPool2d(2),
            torch.nn.Conv2d(32, 64, 3, padding=1),
            torch.nn.ReLU(inplace=True),
            torch.nn.MaxPool2d(2),
            torch.nn.Conv2d(64, 128, 3, padding=1),
            torch.nn.ReLU(inplace=True),
            torch.nn.MaxPool2d(2),
            torch.nn.Conv2d(128, 16, 1, padding=0),
            torch.nn.ReLU(inplace=True)
        )
        self.fc1 = torch.nn.Linear(256, 400)
        self.relu = torch.nn.ReLU(inplace=True)
        self.fc2 = torch.nn.Linear(400, 3 * heads)
        self.fc2.bias.data[:] = 1

    def forward(self, x):
        y = self.avg(x).expand_as(x)
        x = torch.cat((x, y), 1)
        x = self.main(x)
        x = torch.flatten(x, 1)
        x = self.fc2(self.relu(self.fc1(x)))
        return x


def _hook(module, input_):
    print(*input_[0].size())


def _test():
    net = Cerberus(3)
    for m in net.modules():
        m.register_forward_pre_hook(_hook)
    ps = sum(p.numel() for p in net.parameters())
    print(net)
    print(f"{ps / 1000:.1f}K paramters ({ps})")
    x = torch.rand(4, 3, 64, 64)
    y = net(x)
    print(x.size(), "->", y.size())


if __name__ == "__main__":
    _test()
